package kz.shcoder.oneapp;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    public MyTimer timer;
    public int status = 0;
    public final static int TOMATO_TIME = 25;
    public final static int FREE_TIME = 5;
    public final static long MS_IN_M = 60000;
    public final static String[] BTN_TEXT = {"НАДКУСИТЬ", "ПРЕРВАТЬ"};
    public Button b_start;
    public static TextView t_timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    public void pressButton(View v) {
        status = (status == 1)?0:1;
        b_start = (Button)findViewById(R.id.start_b);
        t_timer = (TextView)findViewById(R.id.timer);
        startTimer(status);
    }

    public void startTimer(int st){
        switch (st){
            case 0:
                b_start.setText(BTN_TEXT[0]);
                timer.cancel();
                t_timer.setText(TOMATO_TIME+":00");
                break;
            case 1:
                timer = new MyTimer(TOMATO_TIME*MS_IN_M,1000,TOMATO_TIME);
                b_start.setText(BTN_TEXT[1]);
                timer.start();
                break;
            case 2:
                timer = new MyTimer(FREE_TIME*MS_IN_M,1000, FREE_TIME);
                b_start.setText(BTN_TEXT[1]);
                timer.start();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return false;
//        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}


