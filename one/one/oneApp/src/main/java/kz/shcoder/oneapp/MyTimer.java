package kz.shcoder.oneapp;

import android.os.CountDownTimer;
import android.util.Log;



/**
 * Created by root on 2/16/14.
 */
public class MyTimer extends CountDownTimer{

    private int minute;
    private int seconds = 60;
    private String zero;

    public MyTimer(long millisInFuture, long countDownInterval, int min){
        super(millisInFuture, countDownInterval);
        minute = min;
    }

    @Override
    public void onFinish(){
        // Do something...
    }

    public void onTick(long millisUntilFinished){
        String ms = Long.toString(millisUntilFinished);
        if(seconds == 0){
            minute--;
            seconds = 60;
        }
        seconds--;
        zero = seconds<=9?"0":"";
        MainActivity.t_timer.setText(minute+":"+zero+seconds);
        Log.v("->>",ms);
    }

}